INSERT IGNORE INTO role (role_Id,role) VALUES (1,'USER');
INSERT IGNORE INTO role (role_Id,role) VALUES (2,'INSPECTOR');
INSERT IGNORE INTO status (status_id,name) VALUES (1,'CREATED');
INSERT IGNORE INTO status  (status_id,name) VALUES (2,'SUBMITTED');
INSERT IGNORE INTO status  (status_id,name)VALUES (3,'ACCEPTED');
INSERT IGNORE INTO status  (status_id,name) VALUES (4,'NOT_ACCEPTED');


INSERT IGNORE INTO app_user (first_name, last_name, email, password, role_Id) VALUES (  'inspector1', 'inspector1', 'inspector1@gmail.com', '$2a$10$KreVsfsidoLmFNQIOM59zu80EOIJOR97nxOIL/ZMwRnjFplchBLsO', 2 );
INSERT IGNORE INTO app_user (first_name, last_name, email, password, role_Id) VALUES (  'user1', 'user1', 'user1@gmail.com', '$2a$10$YT16RQy4vZoJEZZZ.7z26eHFmnRDEXgpoF/RTx1Pg.ItqEJrGlyxa', 1 );
