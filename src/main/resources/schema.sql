CREATE TABLE IF NOT EXISTS `role` (
  role_id BIGINT NOT NULL,
   `role` VARCHAR(255) NULL,
   CONSTRAINT pk_role PRIMARY KEY (role_id)
);

CREATE TABLE IF NOT EXISTS status (
  status_id BIGINT NOT NULL,
   name VARCHAR(255) NULL,
   CONSTRAINT pk_status PRIMARY KEY (status_id)
);

CREATE TABLE IF NOT EXISTS app_user (
  user_id BIGINT AUTO_INCREMENT NOT NULL,
   first_name VARCHAR(20) NOT NULL,
   last_name VARCHAR(20) NOT NULL,
   email VARCHAR(45) NOT NULL,
   password VARCHAR(64) NOT NULL,
   role_id BIGINT NULL,
  PRIMARY KEY (user_id),
  UNIQUE KEY uc_app_user_email (email),
  KEY FK_APP_USER_ON_ROLEID (role_id),
  CONSTRAINT FK_APP_USER_ON_ROLEID FOREIGN KEY (role_id) REFERENCES role (role_id)
);

CREATE TABLE IF NOT EXISTS report (
  id BIGINT AUTO_INCREMENT NOT NULL,
   type VARCHAR(255),
   title VARCHAR(255) NOT NULL,
   `description` VARCHAR(255) NOT NULL,
   calculation_value FLOAT NOT NULL,
   user_id BIGINT NOT NULL,
   created_date datetime NULL,
   submitted_date datetime NULL,
   examined_date datetime NULL,
   tax INT NOT NULL,
   notes VARCHAR(255) NULL,
   status BIGINT NULL,
  PRIMARY KEY (id),
  KEY FK_REPORT_ON_STATUS (status),
  KEY FK_REPORT_ON_USER (user_id),
  CONSTRAINT FK_REPORT_ON_STATUS FOREIGN KEY (status) REFERENCES status (status_id),
  CONSTRAINT FK_REPORT_ON_USER FOREIGN KEY (user_id) REFERENCES app_user (user_id)
);

