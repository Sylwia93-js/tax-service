package com.epam.taxservice.util;

import lombok.experimental.UtilityClass;

import java.util.regex.Pattern;

@UtilityClass
public class Parser {

    private static final Pattern PATTERN = Pattern.compile("-?\\d+(\\.\\d+)?");

    public static int parsePage(String page) {
        int parsePage = 1;
        if (isNumeric(page)) {
            parsePage = Integer.parseInt(page);
        }
        return parsePage;
    }


    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        return PATTERN.matcher(strNum).matches();
    }

    public static String parseSortDir(String sortDir) {
        if (sortDir.equalsIgnoreCase("asc") || sortDir.equalsIgnoreCase("desc")) {
            return sortDir;
        } else {
            return "asc";
        }
    }

    public static String parseSortFieldUser(String sortField) {
        if (sortField.equals("userId") ||
                sortField.equals("firstName") ||
                sortField.equals("lastName")) {
            return sortField;
        } else return "firstName";
    }

    public static String parseSortFieldReport(String sortField) {
        if (sortField.equals("type") ||
                sortField.equals("title") ||
                sortField.equals("reportStatus") ||
                sortField.equals("submittedDate") ||
                sortField.equals("createdDate") ||
                sortField.equals("examinedDate")) {
            return sortField;
        } else return "type";
    }

}
