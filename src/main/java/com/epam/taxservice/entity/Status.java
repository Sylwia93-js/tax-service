package com.epam.taxservice.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Getter
@Setter
@Entity
@Table(name = "status")
@NoArgsConstructor
@Transactional
public class Status {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "statusId")
    private Long statusId;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "reportStatus", cascade = CascadeType.ALL)
    private Collection<Report> reports;

    public Status(String name) {
        this.name = name;
    }

    public Status(Long statusId) {
        this.statusId = statusId;
    }


}













