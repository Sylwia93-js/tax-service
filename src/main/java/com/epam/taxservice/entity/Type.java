package com.epam.taxservice.entity;

import lombok.Getter;

@Getter
public enum Type {

    A("A"),
    B("B");

    private final String name;

    Type(String name){
        this.name = name;
    }


}
