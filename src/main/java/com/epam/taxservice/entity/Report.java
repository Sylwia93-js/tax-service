package com.epam.taxservice.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@NoArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "report")
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private Type type;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String description;

    @Column(name = "calculationValue", nullable = false)
    private float value;

    @ManyToOne
    @JoinColumn(name = "user_Id", nullable = false)
    private User user;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDateTime createdDate;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDateTime submittedDate;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDateTime examinedDate;

    @Column(nullable = false)
    private int tax;

    private String notes;

    @ManyToOne
    @JoinColumn(name = "status")
    @ToString.Exclude
    private Status reportStatus;

    public Report(String title, String description, float value, User user, LocalDateTime createdDate, int tax, Status reportStatus) {
        this.title = title;
        this.description = description;
        this.value = value;
        this.user = user;
        this.createdDate = createdDate;
        this.tax = tax;
        this.reportStatus = reportStatus;
    }
}
