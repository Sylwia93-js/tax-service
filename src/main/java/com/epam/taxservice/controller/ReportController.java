package com.epam.taxservice.controller;

import com.epam.taxservice.entity.Report;
import com.epam.taxservice.entity.User;
import com.epam.taxservice.exception.InvalidReportException;
import com.epam.taxservice.service.ReportReader;
import com.epam.taxservice.service.ReportService;
import com.epam.taxservice.util.Parser;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;


@Controller
@RequiredArgsConstructor
@RequestMapping("/report")
public class ReportController {

    private final ReportService reportService;
    private final ReportReader reportReader;


    @GetMapping("/upload")
    public String createReportForm() {
        return "report/upload";
    }

    @PostMapping("/add")
    public String addReport(@RequestParam("file") MultipartFile file,
                            Model model) {
        try {
            var report = reportReader.read(file);
            var loggedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            reportService.addReport(report, loggedUser.getUserId());
            return "report/add";
        } catch (InvalidReportException e) {
            model.addAttribute("message", e.getMessage());
            return "report/upload";
        }
    }

    @GetMapping("/list")
    public String getFromUser(Model model,
                              @Param("keyword") String keyword,
                              @RequestParam(name = "page", defaultValue = "1") String page) {

        int parsePage = Parser.parsePage(page);
        var loggedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Page<Report> reports;
        if (keyword != null && !keyword.isEmpty()) {
            reports = reportService.findByUserIdByKeyword(loggedUser.getUserId(), keyword, parsePage);
        } else {
            reports = reportService.listAllByPageByUser(loggedUser.getUserId(), parsePage);
        }

        model.addAttribute("page", parsePage);
        model.addAttribute("totalReports", reports.getTotalElements());
        model.addAttribute("totalPages", reports.getTotalPages());
        model.addAttribute("keyword", keyword);

        model.addAttribute("reports", reports.getContent());
        return "report/list";
    }

    @GetMapping("/all")
    public String getAll(Model model,
                         @RequestParam(name = "page", defaultValue = "1") String page,
                         @RequestParam(name = "sortField", defaultValue = "reportStatus") String sortField,
                         @RequestParam(name = "sortDir", defaultValue = "desc") String sortDir) {

        int parsePage = Parser.parsePage(page);
        String parseSortDir = Parser.parseSortDir(sortDir);
        String parseSortFieldReport = Parser.parseSortFieldReport(sortField);
        Page<Report> pageReports = reportService.listAll(parsePage, parseSortFieldReport, parseSortDir);

        model.addAttribute("page", parsePage);
        model.addAttribute("totalReports", pageReports.getTotalElements());
        model.addAttribute("totalPages", pageReports.getTotalPages());
        model.addAttribute("sortField", parseSortFieldReport);
        model.addAttribute("sortDirt", parseSortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
        model.addAttribute("reports", pageReports.getContent());
        return "inspector/all-reports";
    }

    @GetMapping("/all/{userId}")
    public String getByUser(@PathVariable(name = "userId") Long userId,
                            Model model,
                            @RequestParam(name = "page", defaultValue = "1") String page,
                            @RequestParam(name = "sortField", defaultValue = "title") String sortField,
                            @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir) {
        int parsePage = Parser.parsePage(page);
        String parseSortDir = Parser.parseSortDir(sortDir);
        String parseSortFieldReport = Parser.parseSortFieldReport(sortField);
        Page<Report> pageReports = reportService.findByUserId(userId, parsePage, parseSortFieldReport, parseSortDir);

        model.addAttribute("page", parsePage);
        model.addAttribute("totalReports", pageReports.getTotalElements());
        model.addAttribute("totalPages", pageReports.getTotalPages());
        model.addAttribute("sortField", parseSortFieldReport);
        model.addAttribute("sortDirt", parseSortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
        model.addAttribute("reports", pageReports.getContent());
        return "inspector/by-user";

    }

    @GetMapping("/submitted")
    public String getSubmitted(Model model,
                               @RequestParam(name = "page", defaultValue = "1") String page,
                               @RequestParam(name = "sortField", defaultValue = "title") String sortField,
                               @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir) {

        int parsePage = Parser.parsePage(page);
        String parseSortDir = Parser.parseSortDir(sortDir);
        String parseSortFieldReport = Parser.parseSortFieldReport(sortField);

        Page<Report> pageReports = reportService.findByStatusPage("SUBMITTED", parsePage, parseSortFieldReport, parseSortDir);


        model.addAttribute("page", parsePage);
        model.addAttribute("totalReports", pageReports.getTotalElements());
        model.addAttribute("totalPages", pageReports.getTotalPages());
        model.addAttribute("sortField", parseSortFieldReport);
        model.addAttribute("sortDirt", parseSortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
        model.addAttribute("reports", pageReports.getContent());
        return "inspector/submitted";
    }


    @GetMapping("/rejected")
    public String getRejected(Model model,
                              @RequestParam(name = "page", defaultValue = "1") String page,
                              @RequestParam(name = "sortField", defaultValue = "title") String sortField,
                              @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir) {

        int parsePage = Parser.parsePage(page);
        String parseSortDir = Parser.parseSortDir(sortDir);
        String parseSortFieldReport = Parser.parseSortFieldReport(sortField);

        Page<Report> pageReports = reportService.findByStatusPage("NOT_ACCEPTED", parsePage, parseSortFieldReport, parseSortDir);

        model.addAttribute("page", parsePage);
        model.addAttribute("totalReports", pageReports.getTotalElements());
        model.addAttribute("totalPages", pageReports.getTotalPages());
        model.addAttribute("sortField", parseSortFieldReport);
        model.addAttribute("sortDirt", parseSortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
        model.addAttribute("reports", pageReports.getContent());
        return "inspector/rejected";
    }

    @GetMapping("/archives")
    public String getAccepted(Model model,
                              @RequestParam(name = "page", defaultValue = "1") String page,
                              @RequestParam(name = "sortField", defaultValue = "title") String sortField,
                              @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir) {

        User user = (User) SecurityContextHolder.getContext().
                getAuthentication().getPrincipal();
        Page<Report> pageReports = null;
        int parsePage = Parser.parsePage(page);
        String parseSortDir = Parser.parseSortDir(sortDir);
        String parseSortFieldReport = Parser.parseSortFieldReport(sortField);
        if (user.getRole().getName().equals("INSPECTOR")) {
            pageReports = reportService.findByStatusPage("ACCEPTED", parsePage, parseSortFieldReport, parseSortDir);

        } else if (user.getRole().getName().equals("USER")) {

            pageReports = reportService.findByStatusPageBYUser(user.getUserId(), "ACCEPTED", parsePage, parseSortFieldReport, parseSortDir);
        }

        model.addAttribute("page", parsePage);
        assert pageReports != null;
        model.addAttribute("totalReports", pageReports.getTotalElements());
        model.addAttribute("totalPages", pageReports.getTotalPages());
        model.addAttribute("sortField", parseSortFieldReport);
        model.addAttribute("sortDirt", parseSortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
        model.addAttribute("reports", pageReports.getContent());
        return "report/archives";
    }


    @PostMapping("/delete")
    public String delete(@RequestParam(name = "reportId") Long reportId) {
        reportService.deleteReport(reportId);
        return "redirect:/report/list";
    }


    @GetMapping("/view")
    public String showReport(@RequestParam(name = "reportId") Long reportId,
                             Model model) {
        Report report = reportService.findById(reportId);
        model.addAttribute(report);
        return "report/view";
    }

    @PostMapping("/update-view")
    public String showUpdateForm(@RequestParam(name = "reportId") Long reportId,
                                 Model model) {

        Report report = reportService.findById(reportId);
        model.addAttribute("report", report);
        return "report/update";
    }


    @PostMapping("/update")
    public String update(@ModelAttribute Report report,
                         BindingResult result) {

        if (result.hasErrors()) {
            return "report/update";
        }

        reportService.update(report);
        return "redirect:/report/list";
    }


    @PostMapping("/submit")
    public String submit(@RequestParam(name = "reportId") Long reportId) {
        reportService.submit(reportId);
        return "redirect:/report/list";
    }

    @PostMapping("/accept")
    public String accept(@RequestParam(name = "reportId") Long reportId) {
        reportService.accept(reportId);
        return "redirect:/report/submitted";
    }


    @GetMapping("/reject")
    public String getRejectView(@RequestParam(name = "reportId") Long reportId,
                                Model model) {
        Report report = reportService.findById(reportId);
        model.addAttribute("report", report);
        return "report/reject";
    }

    @PostMapping("/reject")
    public String reject(@ModelAttribute Report report,
                         Model model) {

        try {
            reportService.reject(report);

        } catch (InvalidReportException e) {
            model.addAttribute("errorMessage", e.getMessage());
            report.setId(report.getId());
            return "report/reject";
        }

        return "redirect:/report/submitted";
    }


    @GetMapping("/statistics")
    public String seeStatistics(Model model) {

        model.addAttribute("countAll", reportService.countAll());
        model.addAttribute("countCreated", reportService.findByStatus("CREATED").size());
        model.addAttribute("countSubmitted", reportService.findByStatus("SUBMITTED").size());
        model.addAttribute("countAccepted", reportService.findByStatus("ACCEPTED").size());
        model.addAttribute("countNotAccepted", reportService.findByStatus("NOT_ACCEPTED").size());

        return "report/statistics";
    }

}
