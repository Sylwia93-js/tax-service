package com.epam.taxservice.controller;

import com.epam.taxservice.dto.UserDto;
import com.epam.taxservice.exception.DuplicatedUserException;
import com.epam.taxservice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.management.relation.RoleNotFoundException;
import javax.validation.Valid;

@Controller
@RequiredArgsConstructor

public class UserRegistrationController {

    private final UserService userService;


    @GetMapping("/registration-view")
    public String showSignUpForm(Model model) {
        UserDto user = new UserDto();
        model.addAttribute("user", user);
        return "signup-form";
    }


    @PostMapping("/register")
    public String processRegistration(@ModelAttribute("user") @Valid UserDto userDto,
                                      BindingResult bindingResult,
                                      Model model) throws RoleNotFoundException {
        if (bindingResult.hasErrors()) {
            return "signup-form";
        }
        try {
            userService.registerNewUserAccount(userDto);
            return "register-success";
        } catch (DuplicatedUserException ex) {
            model.addAttribute("errorMessage", "User already exists");
            return "signup-form";
        }
    }


    @GetMapping("/account")
    public String getAccount() {
        return "account";
    }
}
