package com.epam.taxservice.controller;

import com.epam.taxservice.entity.User;
import com.epam.taxservice.service.UserService;
import com.epam.taxservice.util.Parser;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {


    private final UserService userService;

    @GetMapping("/list")
    public String getUsers(Model model,
                           @Param("keyword") String keyword,
                           @RequestParam(name = "page", defaultValue = "1") String page) {

        Page<User> pageUser;
        int parsePage = Parser.parsePage(page);
        if (keyword != null && !keyword.isEmpty()) {
            pageUser = userService.findbyKeyword(keyword, parsePage);
        } else {
            pageUser = userService.listAll(parsePage);
        }
        model.addAttribute("page", parsePage);
        model.addAttribute("totalUsers", pageUser.getTotalElements());
        model.addAttribute("totalPages", pageUser.getTotalPages());
        model.addAttribute("listUsers", pageUser.getContent());

        return "user/users";
    }


    @GetMapping("/view")
    public String showUser(@RequestParam(name = "userId") Long userId,
                           Model model) {
        User user = userService.findById(userId);
        model.addAttribute(user);

        return "user/view";
    }


}