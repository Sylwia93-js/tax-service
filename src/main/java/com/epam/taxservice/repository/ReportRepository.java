package com.epam.taxservice.repository;

import com.epam.taxservice.entity.Report;
import com.epam.taxservice.entity.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ReportRepository extends JpaRepository<Report, Long> {

 @Query("SELECT r FROM Report r WHERE r.user.userId = ?1 ")
 Page<Report> findByUser(Long userId, Pageable pageable);

 @Query(value = "SELECT r FROM Report r JOIN Status s ON r.reportStatus=s.statusId WHERE s.name = ?1 ")
 List<Report> findByStatus(String status);

 @Query(value = "SELECT r FROM Report r JOIN Status s ON r.reportStatus=s.statusId WHERE s.name = ?1 ")
 Page<Report> findByStatusPage(String status, Pageable pageable);

 @Query("SELECT r FROM Report r JOIN Status s ON r.reportStatus=s.statusId WHERE r.user.userId =:id  AND  " +
         "   (s.name like %:keyword%  or r.type =:keyword)")
 Page<Report> findbyKeyword(@Param("id") Long userId, @Param("keyword") String keyword, Pageable pageable);

 @Query("SELECT r FROM Report r JOIN Status s ON r.reportStatus=s.statusId WHERE r.user.userId =:id AND s.name =:status ")
 Page<Report> findByStatusPageByUser(@Param("id") Long userId, @Param ("status") String status, Pageable pageable);
}