package com.epam.taxservice.repository;

import com.epam.taxservice.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);

    List<User> findAll(Sort sort);

    @Query(value = "SELECT u FROM User u JOIN Role r ON u.role=r.roleId WHERE r.name = 'USER'")
    Page<User> findAll(Pageable pageable);

    @Query(value = "SELECT u FROM User u JOIN Role r ON u.role=r.roleId WHERE " +
            " u.lastName like %:keyword% or u.firstName like %:keyword%")
    Page<User> findAllByKeyword(String keyword,Pageable pageable);
}