package com.epam.taxservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDto implements Serializable {

    @NotNull
    @Size(
            min = 3,
            max = 20,
            message = "The first name must be between {min} and {max} characters long"
    )
    private String firstName;

    @Size(
            min = 3,
            max = 20,
            message = "The last name must be between {min} and {max} characters long"
    )

    @NotNull
    private String lastName;

    @Email(regexp = ".+@.+\\..+", message = "Please enter a valid email address is correct.")
    @NotNull
    private String email;

    @NotEmpty
    @NotNull
    @Size(
            min = 3,
            max = 20,
            message = "The password must be between {min} and {max} characters long"
    )
    private String password;

    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }

}
