package com.epam.taxservice.service;

import com.epam.taxservice.entity.Report;
import com.epam.taxservice.entity.Status;
import com.epam.taxservice.entity.User;
import com.epam.taxservice.exception.InvalidReportException;
import com.epam.taxservice.exception.ReportNotFoundException;
import com.epam.taxservice.repository.ReportRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;


@Service
@RequiredArgsConstructor
public class ReportService {

    private final ReportRepository reportRepository;
    private final StatusService statusService;
    private final UserService userService;


    public void addReport(Report report, Long userId) {

        validateReport(report);

        Status created = statusService.findByName("CREATED");
        report.setReportStatus(created);

        report.setCreatedDate(LocalDateTime.now());
        User loggedUser = userService.findById(userId);

        report.setUser(loggedUser);
        reportRepository.save(report);
    }

    private void validateReport(Report report) {
        if (report == null) {
            throw new InvalidReportException("Report cannot be empty");
        }
        if (report.getTitle() == null || report.getDescription() == null) {
            throw new InvalidReportException("Title or description cannot be empty");
        }
        if (report.getValue() <= 0) {
            throw new InvalidReportException("Value cannot be less opr equal 0");
        }
        if (report.getTax() <= 0) {
            throw new InvalidReportException("Tax cannot be less or equal 0");
        }
    }


    public void deleteReport(Long reportId) {
        Report report = reportRepository.findById(reportId).orElseThrow();
        reportRepository.delete(report);
    }

    public Report findById(Long reportId) {
        return reportRepository.findById(reportId).orElseThrow(() -> new ReportNotFoundException(" Report not found for id " + reportId));
    }

    public void update(Report updatedReport) {
        Report report = reportRepository.findById(updatedReport.getId())
                .orElseThrow(() -> new ReportNotFoundException(" Report not found for id " + updatedReport.getId()));
        report.setTitle(updatedReport.getTitle());
        report.setDescription(updatedReport.getDescription());
        report.setValue(updatedReport.getValue());
        report.setTax(updatedReport.getTax());
        reportRepository.save(report);
    }

    public void submit(Long reportId) {
        Report report = reportRepository.findById(reportId).orElseThrow(() -> new ReportNotFoundException(" Report not found for id " + reportId));
        Status submitted = statusService.findByName("SUBMITTED");
        report.setReportStatus(submitted);
        report.setSubmittedDate(LocalDateTime.now());
        reportRepository.save(report);
    }

    public void accept(Long reportId) {
        Report report = reportRepository.findById(reportId).orElseThrow(() -> new ReportNotFoundException(" Report not found for id " + reportId));
        Status accepted = statusService.findByName("ACCEPTED");
        report.setReportStatus(accepted);
        report.setExaminedDate(LocalDateTime.now());
        reportRepository.save(report);
    }

    public void reject(Long reportId) {
        Report report = reportRepository.findById(reportId).orElseThrow();
        Status notAccepted = statusService.findByName("NOT_ACCEPTED");
        report.setReportStatus(notAccepted);
        report.setSubmittedDate(null);
        reportRepository.save(report);
    }

    public void reject(Report updatedReport) {
        Report report = reportRepository.findById(updatedReport.getId()).orElseThrow();

        report.setNotes(updatedReport.getNotes());
        if (updatedReport.getNotes().isEmpty()) {
            throw new InvalidReportException("Notes cannot be empty");
        }
        reportRepository.save(report);
    }

    public long countAll() {
        return reportRepository.count();
    }


    public List<Report> findByStatus(String status) {
        return reportRepository.findByStatus(status);
    }

    public Page<Report> findByStatusPage(String status, int pageNumber, String sortField, String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
                Sort.by(sortField).descending();
        Pageable pageable = PageRequest.of(pageNumber - 1, 5, sort);
        return reportRepository.findByStatusPage(status, pageable);
    }

    public Page<Report> findByStatusPageBYUser(Long userId, String status, int pageNumber, String sortField, String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
                Sort.by(sortField).descending();
        Pageable pageable = PageRequest.of(pageNumber - 1, 5, sort);
        return reportRepository.findByStatusPageByUser(userId, status, pageable);
    }

    public Page<Report> listAll(int pageNumber, String sortField, String sortDirection) {

        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
                Sort.by(sortField).descending();
        Pageable pageable = PageRequest.of(pageNumber - 1, 5, sort);
        return this.reportRepository.findAll(pageable);
    }


    public Page<Report> findByUserId(Long userId, int pageNumber, String sortField, String sortDirection) {
        Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
                Sort.by(sortField).descending();
        Pageable pageable = PageRequest.of(pageNumber - 1, 5, sort);
        return reportRepository.findByUser(userId, pageable);
    }

    public Page<Report> findByUserIdByKeyword(Long userId, String keyword, int pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber - 1, 5);
        return reportRepository.findbyKeyword(userId, keyword, pageable);

    }

    public Page<Report> listAllByPageByUser(Long userId, int pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber - 1, 5);
        return this.reportRepository.findByUser(userId, pageable);
    }

}
