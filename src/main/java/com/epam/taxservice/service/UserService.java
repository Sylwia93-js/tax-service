package com.epam.taxservice.service;

import com.epam.taxservice.dto.UserDto;
import com.epam.taxservice.entity.User;
import com.epam.taxservice.exception.DuplicatedUserException;
import com.epam.taxservice.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.management.relation.RoleNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {


    private final UserRepository userRepository;
    private final RoleService roleService;
    private final BCryptPasswordEncoder encoder;


    public User findById(Long userId) {
        return userRepository.findById(userId).orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }


    public Page<User> listAll(int pageNumber) {

        Pageable pageable = PageRequest.of(pageNumber - 1, 5);
        return userRepository.findAll(pageable);
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User with email %s does not exists", email)));
    }

    public Long registerNewUserAccount(UserDto userDto) throws RoleNotFoundException {

        validateUserDoesNotExist(userDto);

        var user = User.builder()
                .password(encoder.encode(userDto.getPassword()))
                .firstName(userDto.getFirstName())
                .lastName(userDto.getLastName())
                .email(userDto.getEmail())
                .role(roleService.findByName("USER"))
                .build();

        userRepository.save(user);

        return user.getUserId();
    }

    private void validateUserDoesNotExist(UserDto userDto) {
        if (userRepository.findByEmail(userDto.getEmail()).isPresent()) {
            throw new DuplicatedUserException(String.format("User with email %s already exists", userDto.getEmail()));
        }
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    public Page<User> findbyKeyword(String keyword, int pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber - 1, 5);
        return userRepository.findAllByKeyword(keyword, pageable);
    }
}
