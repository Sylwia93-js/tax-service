package com.epam.taxservice.service;


import com.epam.taxservice.entity.Report;
import com.epam.taxservice.entity.Type;
import com.epam.taxservice.exception.InvalidReportException;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.Set;


@Service
public class ReportReader {


    private static final Set<String> CONTENT_TYPES = Set.of("application/json", "application/xml", "text/xml");

    public Report read(MultipartFile file) {

        validateFile(file);

        if (Objects.equals(file.getContentType(), "application/json")) {
            return getReportFromJsonContent(file);
        } else
            return getReportFromXMLContent(file);

    }

    private Report getReportFromXMLContent(MultipartFile file) {
        Report report = null;


        if (Objects.requireNonNull(file.getContentType()).equals("text/xml")) {
            try {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document document = builder.parse(file.getInputStream());
                var node = document.getDocumentElement();
                var title = getTagValue("title", node);
                var description = getTagValue("description", node);
                var value = getTagValue("value", node);
                var tax = getTagValue("tax", node);

                report = new Report();
                report.setTitle(title);
                report.setDescription(description);
                report.setValue(Float.parseFloat(value));
                report.setTax(Integer.parseInt(tax));


                validateReportFromXML(report);
                report.setType(Type.A);
                return report;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        }

        return report;
    }

    private void validateReportFromXML(Report report) {
        if (report.getTitle() == null || report.getDescription() == null)
            throw new InvalidReportException("Title or description cannot be empty");

    }

    private static String getTagValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = nodeList.item(0);
        return node.getNodeValue();
    }


    private Report getReportFromJsonContent(MultipartFile file) {
        Gson gson = new Gson();
        Report report = null;
        try (var fileStream = new InputStreamReader(file.getInputStream())) {

            JsonElement elem = new JsonParser().parse(fileStream);
            var jsonObjectFile = gson.toJson(elem);


            report = gson.fromJson(jsonObjectFile, Report.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert report != null;
        report.setType(Type.B);
        return report;
    }

    private void validateFile(MultipartFile file) {
        if (file.isEmpty() || file.getSize() == 0) {
            throw new InvalidReportException("File not uploaded");
        }
        if (!CONTENT_TYPES.contains(file.getContentType())) {
            throw new InvalidReportException("Bad file format");
        }
    }
}
