package com.epam.taxservice.service;


import com.epam.taxservice.entity.Status;
import com.epam.taxservice.exception.StatusNotFoundException;
import com.epam.taxservice.repository.StatusRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StatusService {

    private final StatusRepository statusRepository;

    public Status findByName(String name) {
        return statusRepository.findByName(name).orElseThrow(() -> new StatusNotFoundException("Status not found"));
    }

}
