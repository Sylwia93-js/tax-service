package com.epam.taxservice.exception;

public class InvalidReportException extends RuntimeException {
    public InvalidReportException(String s) {
        super(s);
    }
}
