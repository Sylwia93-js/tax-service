package com.epam.taxservice.controller;

import com.epam.taxservice.entity.Report;
import com.epam.taxservice.entity.User;
import com.epam.taxservice.repository.RoleRepository;
import com.epam.taxservice.repository.UserRepository;
import com.epam.taxservice.service.ReportService;
import com.epam.taxservice.service.UserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;


@SpringBootTest
@AutoConfigureMockMvc
class ReportControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserService userService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    BCryptPasswordEncoder encoder;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ReportService reportService;

    @BeforeEach
    void initUseCase() {
        List<User> users = Arrays.asList(
                new User("testUser", "testUser", "testUser@gmail.com",
                        encoder.encode("testUser"), roleRepository.findByName("USER").get(), null),
                new User("testInspector", "testInspector", "testInspector@gmail.com",
                        encoder.encode("testInspector"), roleRepository.findByName("INSPECTOR").get(), null)
        );
        userRepository.saveAll(users);
    }

    @AfterEach
    public void destroyAll() {
        userRepository.deleteAll();
    }


    @Test
    public void itShouldShowAllReportsWhenInspectorIsLoggedIn() throws Exception {

        userService.findByEmail("testInspector@gmail.com");
        var user = userService.loadUserByUsername("testInspector@gmail.com");

        this.mockMvc
                .perform(
                        get("/report/all")
                                .with(SecurityMockMvcRequestPostProcessors.user(user))
                )
                .andExpect(status().isOk())
                .andExpect(view().name("inspector/all-reports"))
                .andExpect(model().attributeExists("page"))
                .andExpect(model().attributeExists("totalReports"))
                .andExpect(model().attributeExists("totalPages"))
                .andExpect(model().attributeExists("sortField"))
                .andExpect(model().attributeExists("sortDirt"))
                .andExpect(model().attributeExists("reverseSortDir"))
                .andExpect(model().attributeExists("reports"));
    }

    @Test
    public void itShouldRedirectToLoginPageWhenUserIsNotInspector() throws Exception {
        this.mockMvc
                .perform(
                        get("/report/all")
                )
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("http://localhost/login"));

    }


    @Test
    public void itShouldGiveForbiddenStatusWhenUserRoleIsNotInspector() throws Exception {

        userService.findByEmail("testUser@gmail.com");
        var user = userService.loadUserByUsername("testUser@gmail.com");

        this.mockMvc
                .perform(
                        get("/report/all")
                                .with(SecurityMockMvcRequestPostProcessors.user(user))
                )
                .andExpect(status().isForbidden());
    }


    @Test
    public void itShouldNotAllowToPostSubmitReportIfUserIsInspector() throws Exception {
        userService.findByEmail("testInspector@gmail.com");
        var inspector = userService.loadUserByUsername("testInspector@gmail.com");
        this.mockMvc
                .perform(
                        post("/report/submit")
                                .with(SecurityMockMvcRequestPostProcessors.user(inspector))

                )
                .andExpect(status().isForbidden());

    }

    @Test
    public void itShouldRedirectToSubmittedReportsWhenReportIsAccepted() throws Exception {

        userService.findByEmail("testInspector@gmail.com");
        var inspector = userService.loadUserByUsername("testInspector@gmail.com");

        var report = new Report();
        report.setTitle("TestTitle");
        report.setDescription("TestDescription");
        report.setValue((float) 1234567.23);
        report.setTax(18);
        reportService.addReport(report, userService.findByEmail("testInspector@gmail.com").getUserId());
        reportService.submit(report.getId());
        Long id = report.getId();

        this.mockMvc
                .perform(
                        post("/report/accept?reportId=" + id)
                                .with(SecurityMockMvcRequestPostProcessors.user(inspector))
                                .with(csrf())
                )
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/report/submitted"));
    }
    @Test
    public void itShouldShowRejectedReportsWhenInspectorIsLoggedIn() throws Exception {

        userService.findByEmail("testInspector@gmail.com");
        var user = userService.loadUserByUsername("testInspector@gmail.com");

        this.mockMvc
                .perform(
                        get("/report/rejected")
                                .with(SecurityMockMvcRequestPostProcessors.user(user))
                )
                .andExpect(status().isOk())
                .andExpect(view().name("inspector/rejected"))
                .andExpect(model().attributeExists("page"))
                .andExpect(model().attributeExists("totalReports"))
                .andExpect(model().attributeExists("totalPages"))
                .andExpect(model().attributeExists("sortField"))
                .andExpect(model().attributeExists("sortDirt"))
                .andExpect(model().attributeExists("reverseSortDir"))
                .andExpect(model().attributeExists("reports"));
    }
    @Test
    public void itShouldShowSubmittedReportsWhenInspectorIsLoggedIn() throws Exception {

        userService.findByEmail("testInspector@gmail.com");
        var user = userService.loadUserByUsername("testInspector@gmail.com");

        this.mockMvc
                .perform(
                        get("/report/submitted")
                                .with(SecurityMockMvcRequestPostProcessors.user(user))
                )
                .andExpect(status().isOk())
                .andExpect(view().name("inspector/submitted"))
                .andExpect(model().attributeExists("page"))
                .andExpect(model().attributeExists("totalReports"))
                .andExpect(model().attributeExists("totalPages"))
                .andExpect(model().attributeExists("sortField"))
                .andExpect(model().attributeExists("sortDirt"))
                .andExpect(model().attributeExists("reverseSortDir"))
                .andExpect(model().attributeExists("reports"));
    }



    @Test
    public void itShouldShowStatisticsPage() throws Exception {
        userService.findByEmail("testInspector@gmail.com");
        var inspector = userService.loadUserByUsername("testInspector@gmail.com");

        this.mockMvc
                .perform(
                        get("/report/statistics")
                                .with(SecurityMockMvcRequestPostProcessors.user(inspector))
                )
                .andExpect(status().isOk())
                .andExpect(view().name("report/statistics"))
                .andExpect(model().attributeExists("countAll"))
                .andExpect(model().attributeExists("countCreated"))
                .andExpect(model().attributeExists("countSubmitted"))
                .andExpect(model().attributeExists("countAccepted"))
                .andExpect(model().attributeExists("countNotAccepted"));

    }

}