package com.epam.taxservice.controller;

import com.epam.taxservice.dto.UserDto;
import com.epam.taxservice.repository.UserRepository;
import com.epam.taxservice.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
class UserRegistrationControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    @MockBean
    UserService userService;


    @Test
    public void itShouldGiveRegistrationView() throws Exception {
        mockMvc.perform(get("/registration-view").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Register")));
    }

    @Test
    void itShouldGiveErrorWhenUserIsInvalid() throws Exception {
        UserDto userDto = new UserDto("Tomm", "smith", "tom", "123456");
        mockMvc.perform(post("/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(userDto)))
                .andExpect(status().is4xxClientError());
    }

    private String toJson(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void itShouldRegisterUserWhenUserIsValid() throws Exception {
        UserDto userDto = new UserDto("Tomm", "smith", "tom@gmail.com", "123456");
        mockMvc.perform(post("/register").with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(userDto)))
                .andExpect(status().isOk());
    }


    @Test
    public void itShouldGiveStatusFoundWhenLoginFormIsCorrect() throws Exception {
        this.mockMvc.perform(formLogin().user("user1@gmail.com").password("user1"))
                .andDo(print())
                .andExpect(status().isFound());
    }


    @Test
    public void itShouldNotLoginUserWithInvalidLoginAndPassword() throws Exception {

        this.mockMvc.perform(post("/login").param("1111", "1111"))
                .andDo(print())
                .andExpect(status().isForbidden());
    }


}