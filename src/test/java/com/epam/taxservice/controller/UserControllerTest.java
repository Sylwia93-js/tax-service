package com.epam.taxservice.controller;

import com.epam.taxservice.entity.User;
import com.epam.taxservice.repository.RoleRepository;
import com.epam.taxservice.repository.UserRepository;
import com.epam.taxservice.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserService userService;

    @Autowired
    BCryptPasswordEncoder encoder;

    @Autowired
    private RoleRepository roleRepository;


    @Test
    public void itShouldGiveStatus200IfWhenGetUserList() throws Exception {
        this.mockMvc.perform(get("/user/list"))
                .andDo(print())
                .andExpect(status().isOk());
    }


    @Test
    public void itShouldShowAllUsersWhenInspectorIsLoggedIn() throws Exception {
        User inspector = userRepository.findByEmail("inspector1@gmail.com").get();

        var user = userService.loadUserByUsername(inspector.getEmail());

        this.mockMvc
                .perform(
                        get("/user/list")
                                .with(SecurityMockMvcRequestPostProcessors.user(user))
                )
                .andExpect(status().isOk())
                .andExpect(view().name("user/users"))
                .andExpect(model().attributeExists("page"))
                .andExpect(model().attributeExists("totalUsers"))
                .andExpect(model().attributeExists("totalPages"))
                .andExpect(model().attributeExists("listUsers"));

    }
}