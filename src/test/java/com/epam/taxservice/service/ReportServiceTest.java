package com.epam.taxservice.service;

import com.epam.taxservice.entity.Report;
import com.epam.taxservice.entity.User;
import com.epam.taxservice.exception.InvalidReportException;
import com.epam.taxservice.exception.ReportNotFoundException;
import com.epam.taxservice.repository.RoleRepository;
import com.epam.taxservice.repository.StatusRepository;
import com.epam.taxservice.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class ReportServiceTest {

    @Autowired
    private ReportService reportService;
    @Autowired
    private UserService userService;
    @Autowired
    private StatusService statusService;
    @Autowired
    BCryptPasswordEncoder encoder;

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private StatusRepository statusRepository;

    @BeforeEach
    void initUseCase() {

        List<User> users = Arrays.asList(
                new User("testUser", "testUser", "testUser1@gmail.com",
                        encoder.encode("testUser"), roleRepository.findByName("USER").get(), null),
                new User("testInspector", "testInspector", "testInspector1@gmail.com",
                        encoder.encode("testInspector"), roleRepository.findByName("INSPECTOR").get(), null)
        );

        userRepository.saveAll(users);
    }

    @AfterEach
    public void destroyAll() {
        userRepository.deleteAll();
    }

    @Test
    public void itShouldAddReport() {

        var a = reportService.listAll(1, "type", "asc");
        var count = a.stream().count();
        Assertions.assertEquals(0, reportService.listAll(1, "type", "asc").stream().count());

        var report = new Report();
        report.setTitle("TestTitle");
        report.setDescription("TestDescription");
        report.setValue((float) 1234567.23);
        report.setTax(18);

        reportService.addReport(report, userService.findByEmail("testUser1@gmail.com").getUserId());

        Assertions.assertEquals(1, reportService.listAll(1, "type", "asc").stream().count());

        var reports = reportService.findByUserId(userService.findByEmail("testUser1@gmail.com").getUserId(), 1, "type", "asc").getContent();

        var addedReport = reports.stream().filter(x -> x.getId().equals(report.getId())).findFirst();

        Assertions.assertEquals(addedReport.get().getTax(), report.getTax());
        Assertions.assertEquals(addedReport.get().getTitle(), report.getTitle());
        Assertions.assertEquals(addedReport.get().getDescription(), report.getDescription());
    }


    @Test
    public void itShouldThrowsInvalidReportExceptionWhenReportIsInvalid() {

        var report = new Report();
        assertThrows(InvalidReportException.class, () -> reportService.addReport(report, userService.findByEmail("testUser1@gmail.com").getUserId()));

        report.setTitle("TestTitle");
        assertThrows(InvalidReportException.class, () -> reportService.addReport(report, userService.findByEmail("testUser1@gmail.com").getUserId()));

        report.setDescription("TestDescription");
        assertThrows(InvalidReportException.class, () -> reportService.addReport(report, userService.findByEmail("testUser1@gmail.com").getUserId()));

        report.setValue(1234567.23f);
        assertThrows(InvalidReportException.class, () -> reportService.addReport(report, userService.findByEmail("testUser1@gmail.com").getUserId()));

        var report2 = new Report();
        report2.setDescription("TestDescription");
        assertThrows(InvalidReportException.class, () -> reportService.addReport(report2, userService.findByEmail("testUser1@gmail.com").getUserId()));

        var report3 = new Report();
        report3.setValue((float) 1234567.23);
        assertThrows(InvalidReportException.class, () -> reportService.addReport(report3, userService.findByEmail("testUser1@gmail.com").getUserId()));

    }


    @Test
    public void itShouldDeleteReport() {

        var report = new Report();
        report.setTitle("TestTitle");
        report.setDescription("TestDescription");
        report.setValue((float) 1234567.23);
        report.setTax(18);

        reportService.addReport(report, userService.findByEmail("testUser1@gmail.com").getUserId());
        Assertions.assertEquals(1, reportService.listAll(1, "type", "asc").stream().count());

        assertDoesNotThrow(() -> reportService.deleteReport(report.getId()));
        assertThrows(ReportNotFoundException.class, () -> reportService.findById(report.getId()));
    }


    @Test
    public void itShouldUpdateReport() {

        var report = new Report();
        report.setTitle("TestTitle");
        report.setDescription("TestDescription");
        report.setValue((float) 1234567.23);
        report.setTax(18);

        reportService.addReport(report, userService.findByEmail("testUser1@gmail.com").getUserId());
        Assertions.assertEquals(1, reportService.listAll(1, "type", "asc").stream().count());

        report.setTax(10);
        report.setDescription("Another description");
        report.setTitle("Another Title");
        report.setValue((float) 666);
        Assertions.assertEquals(1, reportService.listAll(1, "type", "asc").stream().count());

        assertDoesNotThrow(() -> reportService.update(report));

        var updatedReport = reportService.findById(report.getId());
        assertNotNull(updatedReport);
        assertSame(report.getId(), updatedReport.getId());
        assertSame(report.getDescription(), updatedReport.getDescription());
        assertSame(report.getTitle(), updatedReport.getTitle());
        Assertions.assertEquals(report.getValue(), updatedReport.getValue());
    }


    @Test
    public void itShouldGiveSubmittedStatusWhenReportIsSubmits() {

        var report = new Report();
        report.setTitle("TestTitle");
        report.setDescription("TestDescription");
        report.setValue((float) 1234567.23);
        report.setTax(18);

        reportService.addReport(report, userService.findByEmail("testUser1@gmail.com").getUserId());
        reportService.submit(report.getId());

        var submittedReport = reportService.findById(report.getId());
        Assertions.assertEquals("SUBMITTED", submittedReport.getReportStatus().getName());

    }


    @Test
    public void itShouldGiveAcceptedStatusWhenReportIsAccepts() {

        var report = new Report();
        report.setTitle("TestTitle");
        report.setDescription("TestDescription");
        report.setValue((float) 1234567.23);
        report.setTax(18);

        reportService.addReport(report, userService.findByEmail("testUser1@gmail.com").getUserId());
        reportService.submit(report.getId());
        reportService.accept(report.getId());

        var acceptedReport = reportService.findById(report.getId());
        Assertions.assertEquals("ACCEPTED", acceptedReport.getReportStatus().getName());

    }


    @Test
    public void itShouldGiveNotAcceptedStatusWhenReportIsRejects() {

        var report = new Report();
        report.setTitle("TestTitle");
        report.setDescription("TestDescription");
        report.setValue((float) 1234567.23);
        report.setTax(18);


        reportService.addReport(report, userService.findByEmail("testUser1@gmail.com").getUserId());
        reportService.submit(report.getId());
        reportService.reject(report.getId());
        var rejectedReport = reportService.findById(report.getId());
        Assertions.assertEquals("NOT_ACCEPTED", rejectedReport.getReportStatus().getName());
    }

    @Test
    public void itShouldShowStatistics() {
        List<Report> allReports = new ArrayList<>();
        List<Report> createdReports = new ArrayList<>();
        List<Report> submittedReports = new ArrayList<>();
        List<Report> acceptedReports = new ArrayList<>();
        List<Report> notAcceptedReports = new ArrayList<>();

        Report report1 = new Report();
        report1.setTitle("title");
        report1.setDescription("description");
        report1.setTax(23);
        report1.setValue(100);
        reportService.addReport(report1, userService.findByEmail("testUser1@gmail.com").getUserId());
        allReports.add(report1);
        createdReports.add(report1);

        Report report1a = new Report();
        report1a.setTitle("title");
        report1a.setDescription("description");
        report1a.setTax(23);
        report1a.setValue(100);
        reportService.addReport(report1a, userService.findByEmail("testUser1@gmail.com").getUserId());
        allReports.add(report1a);
        createdReports.add(report1a);


        Report report2 = new Report();
        report2.setTitle("title");
        report2.setDescription("description");
        report2.setTax(23);
        report2.setValue(100);
        reportService.addReport(report2, userService.findByEmail("testUser1@gmail.com").getUserId());
        reportService.submit(report2.getId());
        allReports.add(report2);
        submittedReports.add(report2);

        Report report3 = new Report();
        report3.setTitle("title");
        report3.setDescription("description");
        report3.setTax(23);
        report3.setValue(100);
        reportService.addReport(report3, userService.findByEmail("testUser1@gmail.com").getUserId());
        reportService.submit(report3.getId());
        reportService.accept(report3.getId());
        allReports.add(report3);
        acceptedReports.add(report3);

        Report report3a = new Report();
        report3a.setTitle("title");
        report3a.setDescription("description");
        report3a.setTax(23);
        report3a.setValue(100);
        reportService.addReport(report3a, userService.findByEmail("testUser1@gmail.com").getUserId());
        reportService.submit(report3a.getId());
        reportService.accept(report3a.getId());
        allReports.add(report3a);
        acceptedReports.add(report3a);


        Report report4 = new Report();
        report4.setTitle("title");
        report4.setDescription("description");
        report4.setTax(23);
        report4.setValue(100);
        reportService.addReport(report4, userService.findByEmail("testUser1@gmail.com").getUserId());
        reportService.submit(report4.getId());
        reportService.accept(report4.getId());
        allReports.add(report4);
        acceptedReports.add(report4);

        Report report5 = new Report();
        report5.setTitle("title");
        report5.setDescription("description");
        report5.setTax(23);
        report5.setValue(100);
        reportService.addReport(report5, userService.findByEmail("testUser1@gmail.com").getUserId());
        reportService.submit(report5.getId());
        reportService.accept(report5.getId());
        reportService.reject(report5.getId());
        allReports.add(report5);
        notAcceptedReports.add(report5);

        Assertions.assertEquals(reportService.countAll(), allReports.size());
        Assertions.assertEquals(reportService.findByStatus("CREATED").size(), createdReports.size());
        Assertions.assertEquals(reportService.findByStatus("SUBMITTED").size(), submittedReports.size());
        Assertions.assertEquals(reportService.findByStatus("ACCEPTED").size(), acceptedReports.size());
        Assertions.assertEquals(reportService.findByStatus("NOT_ACCEPTED").size(), notAcceptedReports.size());

    }

}
