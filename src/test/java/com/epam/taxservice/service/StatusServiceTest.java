package com.epam.taxservice.service;

import com.epam.taxservice.exception.StatusNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class StatusServiceTest {

    @Autowired
    private StatusService statusService;


    @Test
    void itShouldDoesNotThrowWhenStatusNameIsCorrect() {
        assertDoesNotThrow(() -> statusService.findByName("CREATED"));
        assertDoesNotThrow(() -> statusService.findByName("SUBMITTED"));
        assertDoesNotThrow(() -> statusService.findByName("ACCEPTED"));
        assertDoesNotThrow(() -> statusService.findByName("NOT_ACCEPTED"));
    }

    @Test
    void itShouldFindStatusWhenStatusNameIsValid() {
        assertNotNull(statusService.findByName("CREATED"));
        assertNotNull(statusService.findByName("SUBMITTED"));
        assertEquals("ACCEPTED", statusService.findByName("ACCEPTED").getName());
        assertEquals("NOT_ACCEPTED", statusService.findByName("NOT_ACCEPTED").getName());
    }


    @Test
    void itShouldThrowsNoSuchElementExceptionWhenStatusNameIsInvalid() {

        assertThrows(StatusNotFoundException.class, () -> statusService.findByName("User"));
        assertThrows(StatusNotFoundException.class, () -> statusService.findByName("Inspector"));
        assertThrows(StatusNotFoundException.class, () -> statusService.findByName("Created"));
        assertThrows(StatusNotFoundException.class, () -> statusService.findByName("Submitted"));
        assertThrows(StatusNotFoundException.class, () -> statusService.findByName("Accepted"));
        assertThrows(StatusNotFoundException.class, () -> statusService.findByName("Not_Accepted"));
    }


}
