package com.epam.taxservice.service;

import com.epam.taxservice.exception.InvalidReportException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;

import static org.junit.jupiter.api.Assertions.assertThrows;


@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class ReportReaderTest {

    @Autowired
    private ReportReader reportReader;

    @Test
    public void itShouldReadJsonReport() throws Exception {
        File file = new File("src/test/resources/ReportFiles/report_ok_json.json");
        FileInputStream input = new FileInputStream(file);
        MultipartFile multipartFile = new MockMultipartFile("report_ok_json.json", "report_ok_json.json", "application/json", input);
        var report = reportReader.read(multipartFile);

        Assertions.assertEquals("report1", report.getTitle());
        Assertions.assertEquals("report 1 description", report.getDescription());
        Assertions.assertEquals(100, report.getValue());
        Assertions.assertEquals(23, report.getTax());
    }

    @Test
    public void itShouldThrowsInvalidReportExceptionWhenJsonIsEmpty() throws Exception {
        File file = new File("src/test/resources/ReportFiles/empty_json.json");
        FileInputStream input = new FileInputStream(file);
        MultipartFile multipartFile = new MockMultipartFile("empty_json.json", "empty_json.json", "application/json", input);
        assertThrows(InvalidReportException.class, () -> reportReader.read(multipartFile));
    }

    @Test
    public void itShouldThrowsInvalidReportExceptionWhenJsonHasWrongContentReport_() throws Exception {
        File file = new File("src/test/resources/ReportFiles/report_ok_json.json");
        FileInputStream input = new FileInputStream(file);
        MultipartFile multipartFile = new MockMultipartFile("report_ok_json.json", "report_ok_json.json", "wrong", input);
        assertThrows(InvalidReportException.class, () -> reportReader.read(multipartFile));
    }


    @Test
    public void itShouldReadXmlReport() throws Exception {
        File file = new File("src/test/resources/ReportFiles/report_ok_xml.xml");
        FileInputStream input = new FileInputStream(file);
        MultipartFile multipartFile = new MockMultipartFile("report_ok_xml.xml", "report_ok_xml.xml", "text/xml", input);
        var report = reportReader.read(multipartFile);

        Assertions.assertEquals("Report demo 1", report.getTitle());
        Assertions.assertEquals("description report 1", report.getDescription());
        Assertions.assertEquals(100, report.getValue());
        Assertions.assertEquals(23, report.getTax());
    }


    @Test
    public void itShouldThrowsInvalidReportExceptionWhenXmlHasWrongContentReport_() throws Exception {
        File file = new File("src/test/resources/ReportFiles/report_nok_xml.xml");
        FileInputStream input = new FileInputStream(file);
        MultipartFile multipartFile = new MockMultipartFile("report_ok_xml.xml", "report_ok_xml.xml", "wrong", input);
        assertThrows(InvalidReportException.class, () -> reportReader.read(multipartFile));
    }


}
