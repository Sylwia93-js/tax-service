package com.epam.taxservice.service;

import com.epam.taxservice.dto.UserDto;
import com.epam.taxservice.exception.DuplicatedUserException;
import com.epam.taxservice.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.management.relation.RoleNotFoundException;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class UserServiceTest {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    UserService userService;
    @Mock
    BCryptPasswordEncoder encoder;

    @BeforeEach
    public void destroyAll() {
        userRepository.deleteAll();
    }

    @Test
    public void itShouldSaveUser() throws RoleNotFoundException {
        String password = encoder.encode("sylwia");
        String email = "sylwia@gmail.com";
        UserDto user = new UserDto();
        user.setEmail(email);
        user.setFirstName("sylwia");
        user.setLastName("smith");
        user.setPassword("sylwia");

        var userId = userService.registerNewUserAccount(user);
        Assertions.assertNotNull(user);
        var foundUser = userService.findById(userId);
        Assertions.assertEquals(user.getEmail(), foundUser.getEmail());
        Assertions.assertEquals(user.getFirstName(), foundUser.getFirstName());
        Assertions.assertEquals(user.getLastName(), foundUser.getLastName());
        Assertions.assertNotEquals(user.getPassword(), foundUser.getPassword());
    }


    @Test
    public void itShouldCheckIfUserExists() throws RoleNotFoundException {
        String password = encoder.encode("sylwia");
        String email = "sylwia@gmail.com";
        UserDto user = new UserDto();
        user.setEmail(email);
        user.setFirstName("sylwia");
        user.setLastName("smith");
        user.setPassword("sylwia");
        var userId = userService.registerNewUserAccount(user);

        UserDto user2 = new UserDto();
        user2.setEmail(email);
        user2.setFirstName("sylwia");
        user2.setLastName("smith");
        user2.setPassword("sylwia");

        assertThrows(DuplicatedUserException.class, () -> userService.registerNewUserAccount(user2));
    }

    @Test
    public void itShouldFindAllUsers() throws RoleNotFoundException {

        Assertions.assertEquals(0, (long) userService.findAll().size());

        String password = encoder.encode("sylwia");
        String email = "sylwia@gmail.com";
        UserDto user = new UserDto();
        user.setEmail(email);
        user.setFirstName("sylwia");
        user.setLastName("smith");
        user.setPassword("sylwia");

        var userId = userService.registerNewUserAccount(user);
        Assertions.assertNotNull(user);

        Assertions.assertEquals(1, (long) userService.findAll().size());
    }

    @Test
    public void itShouldLoadUserByUsername() throws RoleNotFoundException {

        String password = encoder.encode("sylwia");
        String email = "sylwia@gmail.com";
        UserDto userDto = new UserDto();
        userDto.setEmail(email);
        userDto.setFirstName("sylwia");
        userDto.setLastName("smith");
        userDto.setPassword("sylwia");

        var userId = userService.registerNewUserAccount(userDto);
        Assertions.assertNotNull(userDto);
        var user = userService.loadUserByUsername(userDto.getEmail());
        Assertions.assertEquals(userDto.getEmail(), user.getUsername());
        Assertions.assertNotEquals(userDto.getPassword(), user.getPassword());

    }


}