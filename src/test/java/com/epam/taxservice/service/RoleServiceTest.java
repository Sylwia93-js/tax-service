package com.epam.taxservice.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.management.relation.RoleNotFoundException;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class RoleServiceTest {

    @Autowired
    private RoleService roleService;


    @Test
    public void itShouldFindRoleByName() {
        assertDoesNotThrow(() -> roleService.findByName("USER"));
        assertDoesNotThrow(() -> roleService.findByName("INSPECTOR"));

        try {
            var a = roleService.findByName("USER");

            assertNotNull(roleService.findByName("USER"));
            assertNotNull(roleService.findByName("INSPECTOR"));
            assertEquals("USER", roleService.findByName("USER").getName());
            assertEquals("INSPECTOR", roleService.findByName("INSPECTOR").getName());

        } catch (RoleNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    void itShouldThrowsRoleNotFoundExceptionWhenRoleNameIsInvalid() {
        assertThrows(RoleNotFoundException.class, () -> roleService.findByName(""));
        assertThrows(RoleNotFoundException.class, () -> roleService.findByName("User"));
        assertThrows(RoleNotFoundException.class, () -> roleService.findByName("Inspector"));
    }


}



