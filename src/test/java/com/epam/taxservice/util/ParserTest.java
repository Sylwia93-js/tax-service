package com.epam.taxservice.util;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class ParserTest {

    @Test
    void itShouldParsingNumber() {
        assertEquals(1, Parser.parsePage("1"));
        assertEquals(12345, Parser.parsePage("12345"));
    }

    @Test
    void itShouldParsingStringToDefaultValue1() {
        assertEquals(1, Parser.parsePage("test"));
    }

    @Test
    void itShouldCheckIsStringIsNumeric() {
        assertTrue(Parser.isNumeric("123"));
        assertFalse(Parser.isNumeric("test"));
    }

    @Test
    void itShouldParsingToSortDir() {
        assertEquals("asc", Parser.parseSortDir("test"));
        assertEquals("desc", Parser.parseSortDir("desc"));
    }

    @Test
    void itShouldParseSortFieldUser() {
        assertEquals("userId", Parser.parseSortFieldUser("userId"));
        assertEquals("firstName", Parser.parseSortFieldUser("test"));
    }

    @Test
    void itShouldParseSortFieldReport() {
        assertEquals("title", Parser.parseSortFieldReport("title"));
        assertEquals("type", Parser.parseSortFieldReport("test"));
    }
}