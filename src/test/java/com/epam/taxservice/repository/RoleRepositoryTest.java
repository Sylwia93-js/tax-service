package com.epam.taxservice.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class RoleRepositoryTest {

    @Autowired
    private RoleRepository roleRepository;


    @Test
    void itShouldFindRoleName() {
        assertDoesNotThrow(() -> roleRepository.findByName("USER").get());
        assertDoesNotThrow(() -> roleRepository.findByName("INSPECTOR").get());
        assertNotNull(roleRepository.findByName("USER").get());
        assertNotNull(roleRepository.findByName("INSPECTOR").get());
        assertEquals("USER", roleRepository.findByName("USER").get().getName());
        assertEquals("INSPECTOR", roleRepository.findByName("INSPECTOR").get().getName());
    }

    @Test
    void ItShouldThrowsNoSuchElementExceptionWhenNameIsInvalid() {
        assertThrows(NoSuchElementException.class, () -> roleRepository.findByName("User").get());
        assertThrows(NoSuchElementException.class, () -> roleRepository.findByName("Inspector").get());

    }

    @Test
    void itShouldFindRoleId() {
        assertDoesNotThrow(() -> roleRepository.findById(1L));
        assertDoesNotThrow(() -> roleRepository.findById(2L));
    }


}
