package com.epam.taxservice.repository;

import com.epam.taxservice.entity.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    BCryptPasswordEncoder encoder;

    @BeforeEach
    void initUseCase() {

        List<User> users = Arrays.asList(
                new User("test", "test", "test@gmail.com",
                        encoder.encode("test"), roleRepository.findByName("USER").get(), null),
                new User("testTest", "testTest", "testTest@gmail.com",
                        encoder.encode("test"), roleRepository.findByName("USER").get(), null)
        );

        userRepository.saveAll(users);
    }

    @AfterEach
    public void destroyAll() {
        userRepository.deleteAll();
    }

    @Test
    void itShouldSaveAllUsers() {
        List<User> users = Arrays.asList(
                new User("test", "test", "test4@gmail.com",
                        encoder.encode("test"), roleRepository.findByName("USER").get(), null),
                new User("test", "test", "test5@gmail.com",
                        encoder.encode("test"), roleRepository.findByName("USER").get(), null),
                new User("test", "test", "test6@gmail.com",
                        encoder.encode("test"), roleRepository.findByName("USER").get(), null)
        );
        Iterable<User> allUsers = userRepository.saveAll(users);

        AtomicInteger validIdFound = new AtomicInteger();
        allUsers.forEach(user -> {
            if (user.getUserId() > 0) {
                validIdFound.getAndIncrement();
            }
        });

        assertThat(validIdFound.intValue()).isEqualTo(3);
    }

    @Test
    void itShouldFindAllUsers() {
        assertEquals(2, userRepository.findAll().size());
    }


    @Test
    void itShouldFindUserByEmail() {
        String email = "test@gmail.com";
        Optional<User> user = userRepository.findByEmail(email);
        assertThat(email).isEqualTo(user.get().getEmail());
    }

    @Test
    void itShouldNotFindUserById() {
        Optional<User> user = userRepository.findById(4L);
        assertFalse(user.isPresent());
    }


    @Test
    void itShouldEncodePassword() {
        assertDoesNotThrow(() -> encoder.encode("Password"));
        assertNotNull(encoder.encode("Password"));
        assertNotEquals(encoder.encode("Password"), "");
    }

}