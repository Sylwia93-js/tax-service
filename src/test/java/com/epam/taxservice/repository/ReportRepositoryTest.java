package com.epam.taxservice.repository;

import com.epam.taxservice.entity.Report;
import com.epam.taxservice.entity.Status;
import com.epam.taxservice.entity.User;
import com.epam.taxservice.service.RoleService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest
class ReportRepositoryTest {
    @Autowired
    private ReportRepository reportRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private StatusRepository statusRepository;
    @Autowired
    BCryptPasswordEncoder encoder;
    @Autowired
    RoleService roleService;
    @Autowired
    private RoleRepository roleRepository;


    @BeforeEach
    void initUseCase() {

        List<User> users = List.of(
                new User("test", "test", "test@gmail.com",
                        encoder.encode("test"), roleRepository.findByName("USER").get(), null)
        );

        userRepository.saveAll(users);
    }

    @AfterEach
    public void destroyAll() {
        userRepository.deleteAll();
    }


    @Test
    void itShouldSaveReportAndFindById() {
        Optional<User> user = userRepository.findByEmail("test@gmail.com");
        Optional<Status> created = statusRepository.findByName("CREATED");

        Report report = new Report("report1", "report 1 description", 120, user.get(), LocalDateTime.now(), 23, created.get());
        reportRepository.save(report);

        Optional<Report> expected = reportRepository.findById(report.getId());

        assertTrue(expected.isPresent());
        assertEquals(report.getId(), expected.get().getId());
        assertEquals(report.getTitle(), expected.get().getTitle());
        assertEquals(report.getTax(), expected.get().getTax());
        assertEquals(report.getDescription(), expected.get().getDescription());
        assertEquals(report.getValue(), expected.get().getValue());
    }

    @Test
    void itShouldNotFindById() {
        assertTrue(reportRepository.findById(2L).isEmpty());
    }


}