package com.epam.taxservice.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class StatusRepositoryTest {

    @Autowired
    private StatusRepository statusRepository;


    @Test
    void itShouldFindStatusByName() {
        assertTrue(statusRepository.findByName("CREATED").isPresent());
        assertTrue(statusRepository.findByName("SUBMITTED").isPresent());
        assertEquals("ACCEPTED", statusRepository.findByName("ACCEPTED").get().getName());
        assertEquals("NOT_ACCEPTED", statusRepository.findByName("NOT_ACCEPTED").get().getName());
    }


    @Test
    void itShouldThrowsNoSuchElementExceptionWhenStatusIsNotFound() {
        assertThrows(NoSuchElementException.class, () -> statusRepository.findByName("Created").get());
        assertThrows(NoSuchElementException.class, () -> statusRepository.findByName("Submitted").get());
        assertThrows(NoSuchElementException.class, () -> statusRepository.findByName("Accepted").get());
        assertThrows(NoSuchElementException.class, () -> statusRepository.findByName("Not_Accepted").get());
    }


}
